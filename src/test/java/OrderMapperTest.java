import static org.junit.Assert.*;

import java.io.InputStream;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.BeforeClass;
import org.junit.Test;

import com.zh.dao.OrderMapper;
import com.zh.pojo.Order;
import com.zh.pojo.OrderCustomer;

/**
 *@author   zhuhuan
 *@date     2016年8月3日下午3:30:09
 *@version  1.0
*/
public class OrderMapperTest {
	private static SqlSessionFactory sessionFactory;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		InputStream is=UserMapperTest.class.getClassLoader().getResourceAsStream("mybatisconfig.xml");
		sessionFactory=new SqlSessionFactoryBuilder().build(is);
	}
	
	@Test
	public void testFindOrderUser() {
		SqlSession sqlSession=sessionFactory.openSession();
		OrderMapper orderMapper=sqlSession.getMapper(OrderMapper.class);
		List<OrderCustomer> list=orderMapper.findOrderUser();
		System.out.println(list.size());
	}

	@Test
	public void testFindOrderUserResultMap() {
		SqlSession sqlSession=sessionFactory.openSession();
		OrderMapper orderMapper=sqlSession.getMapper(OrderMapper.class);
		List<Order> list=orderMapper.findOrderUserResultMap();
		System.out.println(list.size());
	}
	
	@Test
	public void testFindOrderAndOrderDetails() {
		SqlSession sqlSession=sessionFactory.openSession();
		OrderMapper orderMapper=sqlSession.getMapper(OrderMapper.class);
		List<OrderCustomer> list=orderMapper.findOrderAndOrderDetails();
		System.out.println(list.size());
		//查询结果为4条
	}
	
	@Test
	public void testFindOrderAndOrderDetailsResultMap() {
		SqlSession sqlSession=sessionFactory.openSession();
		OrderMapper orderMapper=sqlSession.getMapper(OrderMapper.class);
		List<Order> list=orderMapper.findOrderAndOrderDetailsResultMap();
		System.out.println(list.size());
		//查询结果为2条
	}
}
