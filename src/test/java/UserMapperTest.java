import static org.junit.Assert.*;

import java.io.InputStream;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import com.zh.pojo.User;

/**
 *@author   zhuhuan
 *@date     2016年7月31日上午10:55:00
 *@version  1.0
*/
public class UserMapperTest {

	@Test
	public void testGetUserById() {
		InputStream is=UserMapperTest.class.getClassLoader().getResourceAsStream("mybatisconfig.xml");
		SqlSessionFactory sessionFactory=new SqlSessionFactoryBuilder().build(is);
		SqlSession session=sessionFactory.openSession();
		User user=session.selectOne("com.zh.pojo.userMapper.getUserById", 1);
		session.close();
		System.out.println(user.toString());	
	}
	
	@Test
	public void testGetUserByName() {
		InputStream is=UserMapperTest.class.getClassLoader().getResourceAsStream("mybatisconfig.xml");
		SqlSessionFactory sessionFactory=new SqlSessionFactoryBuilder().build(is);
		SqlSession session=sessionFactory.openSession();
		List<User> list=session.selectList("com.zh.pojo.userMapper.getUserByName", "%欢%");
		session.close();
		for(User user:list){
			System.out.println(user.toString());
		}	
	}
	
	@Test
	public void testDeleteUserById() {
		InputStream is=UserMapperTest.class.getClassLoader().getResourceAsStream("mybatisconfig.xml");
		SqlSessionFactory sessionFactory=new SqlSessionFactoryBuilder().build(is);
		SqlSession session=sessionFactory.openSession();
		session.delete("com.zh.pojo.userMapper.deleteUserById", 5);
		session.commit();
		session.close();
	}
	
	@Test
	public void testUpdateUser() {
		InputStream is=UserMapperTest.class.getClassLoader().getResourceAsStream("mybatisconfig.xml");
		SqlSessionFactory sessionFactory=new SqlSessionFactoryBuilder().build(is);
		SqlSession session=sessionFactory.openSession();
		User user=new User();
		user.setId(4);
		user.setAge(18);
		user.setName("欢欢");
		user.setBirthday(new Date());
		session.update("com.zh.pojo.userMapper.updateUser",user);
		session.commit();
		session.close();
	}

}
