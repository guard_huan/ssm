import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.zh.dao.UserMapper;
import com.zh.pojo.User;

/**
 *@author   zhuhuan
 *@date     2016年8月13日下午3:52:53
 *@version  1.0
*/
//spring和Mybatis整合测试，用id来查询用户
public class TestSpring {
	@Test
	public void testSpring(){
		 ApplicationContext ac=new ClassPathXmlApplicationContext("springmybatis.xml");
		 /*Mapper接口的bean  id默认为Mapper类名，就是首字母需要小写*/
	     UserMapper userMapper=(UserMapper)ac.getBean("userMapper");
	     User user=userMapper.getUserById(10);
	     System.out.println(user.getName());
	}
}
