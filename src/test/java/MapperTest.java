import static org.junit.Assert.*;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.zh.dao.UserMapper;
import com.zh.pojo.User;
import com.zh.pojo.UserCustomer;
import com.zh.pojo.UserQueryVo;

/**
 *@author   zhuhuan
 *@date     2016年7月31日下午8:53:15
 *@version  1.0
*/
public class MapperTest {
	private static SqlSessionFactory sessionFactory;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		InputStream is=UserMapperTest.class.getClassLoader().getResourceAsStream("mybatisconfig.xml");
		sessionFactory=new SqlSessionFactoryBuilder().build(is);
	}

	//Mapper代理的开发方法 测试
	@Test
	public void testGetUserById() {			
		SqlSession sqlSession=sessionFactory.openSession();
		//MyBatis自动获取UserMapper的对象
		UserMapper userMapper=sqlSession.getMapper(UserMapper.class);
		User user=userMapper.getUserById(1);
		System.out.println(user.getName());
	}
	
	@Test
	public void  testFindUserList(){
		SqlSession sqlSession=sessionFactory.openSession();
		//MyBatis自动获取UserMapper的对象
		UserMapper userMapper=sqlSession.getMapper(UserMapper.class);
		UserCustomer userCustomer=new UserCustomer();
		userCustomer.setAge(23);
		userCustomer.setName("欢");
		UserQueryVo userQueryVo=new UserQueryVo();
		userQueryVo.setUserCustomer(userCustomer);
		List<Integer> ids=new ArrayList<Integer>();
		ids.add(2);
		userQueryVo.setIds(ids);
		List<UserCustomer> list=userMapper.findUserList(userQueryVo);		
	}
	
	@Test
	public void testFindUserCount(){
		SqlSession sqlSession=sessionFactory.openSession();
		//MyBatis自动获取UserMapper的对象
		UserMapper userMapper=sqlSession.getMapper(UserMapper.class);
		UserCustomer userCustomer=new UserCustomer();
		userCustomer.setAge(23);
		userCustomer.setName("欢");
		UserQueryVo userQueryVo=new UserQueryVo();
		userQueryVo.setUserCustomer(userCustomer);
		int i=userMapper.findUserCount(userQueryVo);
		System.out.println(i);
	}
	
	

}
