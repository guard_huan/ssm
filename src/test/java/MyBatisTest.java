import java.io.InputStream;
import java.util.Date;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.zh.pojo.User;

public class MyBatisTest {

	/**
	 * 
	 * @param args
	 * @description  TODO
	 * @date    2016年7月26日上午10:04:28
	 */
	public static void main(String[] args) {
         InputStream is=MyBatisTest.class.getClassLoader().getResourceAsStream("mybatisconfig.xml");
         SqlSessionFactory sessionFactory=new SqlSessionFactoryBuilder().build(is);
		 SqlSession session=sessionFactory.openSession();
		 
	    	/* 从数据库中查询一条记录*/
         /*String statement="com.zh.pojo.userMapper.getUserById";
		 User user=session.selectOne(statement, 4);
		 System.out.println(user.getName());*/
		 
		/* 向数据库中添加一条数据*/
	     String statement="com.zh.pojo.userMapper.addUser";
		 User user=new User();
		 user.setName("朱欢");
		 user.setAge(23);
		 user.setBirthday(new Date());
		 int result=session.insert(statement, user);
		 session.commit();
		 session.close();
		 System.out.println(result);
		 
	}

}

