<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>用户列表界面</title>
</head>
<body>   
	<table>
		<c:forEach items="${userList}" var="userCustomer">
			<tr>
				<td>${userCustomer.id}</td>
				<td>${userCustomer.name}</td>
				<td>${userCustomer.birthday}</td>
				<td>${userCustomer.age}</td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>