package com.zh.dao;

import java.util.List;

import com.zh.pojo.User;
import com.zh.pojo.UserCustomer;
import com.zh.pojo.UserQueryVo;

/**
 *@author   zhuhuan
 *@date     2016年7月31日下午4:44:19
 *@version  1.0
*/
public interface UserMapper {
     public User getUserById(int id);
     public List<UserCustomer> findUserList(UserQueryVo usesrQueryVo);
     public int findUserCount(UserQueryVo usesrQueryVo);
     public List<User> getUserByName(String name);
     public void addUser(User user);
     public void deleteUserById(String id);
     public void updateUser(User user);
}
