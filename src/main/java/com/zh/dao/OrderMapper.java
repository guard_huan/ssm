package com.zh.dao;

import java.util.List;

import com.zh.pojo.Order;
import com.zh.pojo.OrderCustomer;

/**
 *@author   zhuhuan
 *@date     2016年8月3日下午3:06:19
 *@version  1.0
*/
public interface OrderMapper {
	public List<OrderCustomer> findOrderUser();
	public List<Order> findOrderUserResultMap();
	public List<OrderCustomer> findOrderAndOrderDetails();
	public List<Order> findOrderAndOrderDetailsResultMap();
}
