package com.zh.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.zh.pojo.UserCustomer;
import com.zh.service.UserService;


/**
 *@author   zhuhuan
 *@date     2016年8月17日下午12:07:49
 *@version  1.0
*/
@Controller
//为了对url进行分类管理，在controller之前定义访问的根路径，最终访问的curl是根路径+子路径
//例如，/items/queryItems.action 
@RequestMapping("/items")
public class UserController {
	@Autowired
    private UserService userService;
	
	/*一般这里和方法名一致，这样就不会重复，之后调用该方法，就请求/items/queryItems.action*/
	@RequestMapping("/queryItems")
	public ModelAndView queryItems(){
		List<UserCustomer> userList=userService.findUserList(null);
		ModelAndView modelAndView=new ModelAndView();
		modelAndView.addObject("userList", userList);
		modelAndView.setViewName("user/userList");
		return modelAndView;
	}
}
