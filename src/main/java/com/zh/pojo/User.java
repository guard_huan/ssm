package com.zh.pojo;

import java.util.Date;

public class User {
    private int id;
    private String name;
    private int age;
    private Date birthday;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	
	public String toString(){
		System.out.println("id:"+id+" 用户名："+name+" 年龄："+age+" 生日："+birthday);
		return null;
	}
    
}

