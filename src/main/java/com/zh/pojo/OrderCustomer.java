package com.zh.pojo;

import java.util.Date;

/**
 *@author   zhuhuan
 *@date     2016年8月3日下午3:21:24
 *@version  1.0
*/
public class OrderCustomer extends Order {
	/*添加一些必要的用户信息*/
     private String name;
     private Date birthday;
     private int age;
     
    /*添加订单详细信息*/
     private int ordersdetail_id;
     private int items_id;
     private int items_num;
     
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
	public int getOrdersdetail_id() {
		return ordersdetail_id;
	}
	public void setOrdersdetail_id(int ordersdetail_id) {
		this.ordersdetail_id = ordersdetail_id;
	}
	public int getItems_id() {
		return items_id;
	}
	public void setItems_id(int items_id) {
		this.items_id = items_id;
	}
	public int getItems_num() {
		return items_num;
	}
	public void setItems_num(int items_num) {
		this.items_num = items_num;
	}
	
     
}
