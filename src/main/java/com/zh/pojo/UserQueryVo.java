package com.zh.pojo;

import java.util.List;

/**
 *@author   zhuhuan
 *@date     2016年8月2日上午10:52:37
 *@version  1.0
*/
public class UserQueryVo {
    //可以用来包装其它的查询条件，常将其它的pojo引用进来，很少引用单个属性，这样的话，会导致类中有很多的属性
	
	//传入多个用户的id
	private List<Integer> ids;
	private UserCustomer userCustomer;

	
	public List<Integer> getIds() {
		return ids;
	}

	public void setIds(List<Integer> ids) {
		this.ids = ids;
	}

	public UserCustomer getUserCustomer() {
		return userCustomer;
	}

	public void setUserCustomer(UserCustomer userCustomer) {
		this.userCustomer = userCustomer;
	}
	
}
