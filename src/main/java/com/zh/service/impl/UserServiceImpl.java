package com.zh.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zh.dao.UserMapper;
import com.zh.pojo.User;
import com.zh.pojo.UserCustomer;
import com.zh.pojo.UserQueryVo;
import com.zh.service.UserService;

/**
 *@author   zhuhuan
 *@date     2016年8月17日下午12:05:49
 *@version  1.0
*/
@Service
public class UserServiceImpl implements UserService {
    @Autowired
	private UserMapper userMapper;
    
	@Override
	public List<UserCustomer> findUserList(UserQueryVo usesrQueryVo) {		
		return userMapper.findUserList(usesrQueryVo);
	}

}
