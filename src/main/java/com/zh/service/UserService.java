package com.zh.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zh.pojo.UserCustomer;
import com.zh.pojo.UserQueryVo;

/**
 *@author   zhuhuan
 *@date     2016年8月17日下午12:04:58
 *@version  1.0
*/

public interface UserService {
    public List<UserCustomer> findUserList(UserQueryVo usesrQueryVo);
}
